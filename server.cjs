const { response } = require('express');
const express = require('express');
const { request } = require('http');
const fs = require('fs').promises;
const path = require('path');

const app = express();
app.use(express.json());

app.post('/', (request, response) => {

    fs.mkdir(path.join(__dirname, "output", request.body.directoryName))
        .then(() => {
            arrOfFiles = [];
            directoryPath = path.join(__dirname, "output", request.body.directoryName)
            request.body.fileNames.forEach(file => {
                filePath = path.join(directoryPath, file)
                arrOfFiles.push(fs.writeFile(filePath, "Hello"))
            });
            return Promise.all(arrOfFiles);
        })
        .then(() => {
            response
                .setHeader("status", "success")
                .set({ "Context-type": "application/json" })
                .send({ message: "files created" })
        })
        .catch(() => {
            response
                .setHeader("status", "failed")
                .set({ "Context-type": "application/json" })
                .send({ message: "some error" })
        })
})
app.delete('/', (request, response) => {

    deletedFilesArr = [];
    notDeletedFilesArr = [];

    if ((JSON.stringify(request.body).length === 2)) {
        response
            .set({ "Context-type": "application/json" })
            .setHeader("status", "error")
            .send({ message: "no data sent" })
    } else {
        directoryPath = path.join(__dirname, "output", request.body.directoryName);
        fileNamesArr = request.body.fileNames;

        return fs.readdir(directoryPath)
            .then((fileNames) => {
                fileNamesArrPromises = [];

                fileNamesArr.forEach((file) => {
                    if (fileNames.includes(file)) {
                        deletedFilesArr.push(file);
                        fileNamesArrPromises.push(fs.unlink(path.join(directoryPath, file)));
                    }
                    else {
                        notDeletedFilesArr.push(file)
                    }
                });
                if (deletedFilesArr.length === 0) {
                    throw new Error(`some files are still present`);

                } else if (deletedFilesArr.length !== fileNamesArr.length) {
                    response
                        .set({ "Context-type": "application/json" })
                        .send(`some deleted files ${deletedFilesArr} and some remaining files ${notDeletedFilesArr}`);

                } else if (deletedFilesArr.length === fileNamesArr.length) {
                    response
                        .set({ "Context-type": "application/json" })
                        .send(`All files Deleted ${deletedFilesArr}`);
                }
                return Promise.all(fileNamesArrPromises);
            })
            .catch((err) => {
                response
                    .set({ "Context-type": "application/json" })
                    .setHeader("status", "success")
                    .send({ message: "file does not exist" })
            })
    }
});
app.listen(8080, () => {
    console.log(`port listening on 8080`);
})